#pragma once

#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<istream>
#include<fstream>
#include<ostream>
#include<iostream>
#include<string>
#include<vector>
#include<stdexcept>

class Description{
	protected:
		int Ep_number;
		int Ep_duration;
		std::string Ep_name;
	public:
		Description(int, int, std::string);
		Description(Description &);
			void set_Ep_num(int); 
			void set_Ep_dur(int);
			void set_Ep_name(std::string);
			
			int get_Ep_num()const;
			int get_Ep_dur()const;
			std::string get_Ep_name()const;
};

class Episode:public Description {
private:
	int N;
	double sum;
	double max;
public:
	Episode();
	Episode(int, double, double, Description);
	Episode(const Episode&);
	void set_N(int);
	void set_sum(double);
	void set_max(double);

	int get_N() const;
	double get_sum() const;
	double get_max()const;
	
    void addView (double R);
};

class Season{
	friend std::ostream& operator<<(std::ostream&, Season&);
	private:
		std::vector<Episode> eps;
	public:
		Season(const std::vector<Episode>);
		Season(const Season&);
		~Season();

		void add(Episode);
		Episode& operator[](int);
		Season& operator=(const Season&);
		double getAverageRating() const;
		double getTotalViews() const;
		Episode& getBestEpisode();
		void remove(const std::string);
	};

bool operator==(const Episode&, const Episode&);

std::ostream& operator<<(std::ostream&, const Episode&);
std::istream& operator>>(std::istream&, Episode&);
std::ostream& operator<<(std::ostream&, const Description&);

double generateRandomScore();

//Episode** loadEpisodesFromFile(const std::string, int);
std::vector<Episode> loadEpisodesFromFile(std::string);

class IPrinter{
	public:
	IPrinter(){}
	virtual void print(Season& X)=0;
};

class FilePrinter: public IPrinter{
	std::string f;
	public:
	FilePrinter(std::string f): f(f), IPrinter(){}
	virtual void print(Season& X);
};

class ConsolePrinter: public IPrinter{
	public:
	ConsolePrinter(): IPrinter(){}
	virtual void print(Season& X);
};

class EpisodeNotFoundException: public std::runtime_error {
	private:
	std::string name;
	public:
	EpisodeNotFoundException(std::string, std::string);
	std::string getName() const;
};