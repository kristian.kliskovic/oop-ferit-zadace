#include "Episodes.h"

//geteri i seteri
void Description::set_Ep_num(int q) { Ep_number=q; }
void Description::set_Ep_dur(int s) {Ep_duration=s;}
void Description::set_Ep_name(std::string x) {Ep_name=x;}
			
int Description::get_Ep_num()const{ return Ep_number;}
int Description::get_Ep_dur()const{ return Ep_duration;}
			
void Episode::set_N(int q) { N=q; }
void Episode::set_sum(double s) {sum=s;}
void Episode::set_max(double m) {max=m;}

int Episode::get_N() const{ return N;}
double Episode::get_sum() const{ return sum;}
double Episode::get_max()const{ return max;}
std::string Description::get_Ep_name()const{ return Ep_name;}

//kons i des
Description::Description(int Ep_numberq=0, int Ep_durationq=0, std::string Ep_nameq=""): Ep_number(Ep_numberq), Ep_duration(Ep_durationq), Ep_name(Ep_nameq){}
Description::Description(Description &q): Ep_number(q.Ep_number),  Ep_duration(q.Ep_duration),  Ep_name(q.Ep_name){}
		

Episode::Episode() : N(0), sum(0), max(0), Description(0,0,"") {}
Episode::Episode(int Nq, double Sumq, double maxq, Description q) : N(Nq), sum(Sumq), max(maxq), Description(q){}
Episode::Episode(const Episode& ref): N(ref.N), sum(ref.sum), max(ref.max), Description(ref.Ep_number, ref.Ep_duration, ref.Ep_name){}


Season::Season(const Season& ref){
	eps=ref.eps;
}

Season::Season(const std::vector<Episode> X){
	eps=X;
}

Season::~Season(){
	eps.clear();
}


//operatori
bool operator==(const Episode &lhs, const Episode &rhs){
	//usporedit cu sve i samo ako se sve podudara vraca 1
	if(lhs.get_Ep_dur()==rhs.get_Ep_dur()){
		if(lhs.get_Ep_name()==rhs.get_Ep_name()){
			if(lhs.get_Ep_num()==rhs.get_Ep_num()){
				if(lhs.get_max()==rhs.get_max()){
					if(lhs.get_N()==rhs.get_N()){
						if(lhs.get_sum()==rhs.get_sum()){
							return 1;
						}
					}
				}
			}
		}
	}
	return 0;
}

Season& Season::operator=(const Season& rhs){
	if(this!=&rhs){
		eps=rhs.eps;
	}
}

Episode& Season::operator[](int x){
	return this->eps[x];
}

std::ostream& operator<<(std::ostream& out, const Description& rhs){ return out<<rhs.get_Ep_num()<<"\t"<<rhs.get_Ep_name()<<"\t"<<rhs.get_Ep_dur(); }
std::ostream& operator<<(std::ostream& out, const Episode& rhs){ return out<<rhs.get_Ep_num()<<"\t"<<rhs.get_Ep_dur()<<"\t"<<rhs.get_Ep_name()<<"\t"<<rhs.get_sum()<<"\t"<<rhs.get_N()<<"\t"<<rhs.get_max();}

std::ostream& operator<<(std::ostream& stream, Season& X){
	for(int i=0;i<X.eps.size();i++){
		stream<<X[i]<<std::endl;
	}
	return stream;
}

std::istream& operator>>(std::istream& in, Episode& rhs){
	int tempi;
	double tempd;
	std::string temps;
	in>>tempi;
	rhs.set_N(tempi); 
	in.ignore(1);
	in>>tempd;
	rhs.set_sum(tempd);
	in.ignore(1);
	in>>tempd;
	rhs.set_max(tempd);
	in.ignore(1);
	in>>tempi;
	rhs.set_Ep_num(tempi);
	in.ignore(1);
	in>>tempi;
	rhs.set_Ep_dur(tempi);
	in.ignore(1);
	getline(in, temps);
	rhs.set_Ep_name(temps);
	return in;
}

void Episode::addView(double R){
	N++;
	sum += R;
	if (R > max) max = R;
}
 

double generateRandomScore(){ return ((1.0 * rand()) / RAND_MAX * 10); }

double Season::getAverageRating() const{
	double sums=0;
	int Ns=0;
	for(int i=0;i<eps.size();i++){
		sums+=eps[i].get_sum();
		Ns+=eps[i].get_N();
	}
	return sums/Ns;
}

void Season::add(Episode X){
	eps.push_back(X);
}

double Season::getTotalViews() const{
	int tot=0;
	for(int i=0;i<eps.size();i++){
		tot+=eps[i].get_N();
	}
	return tot;
}

Episode& Season::getBestEpisode(){
	double max=eps[0].get_max();
	int q=0;
	for(int i=0;i<eps.size();i++){
		if(eps[i].get_max()>max){
			max=eps[i].get_max();
			q=i;
		}
	}
	return eps[q];
}



std::vector<Episode> loadEpisodesFromFile(const std::string f){
	int num=0;
	std::string line;
    std::ifstream dat(f);
    if(dat.is_open()){
        while(!dat.eof()){
            getline(dat,line);
            num++;
        }
    }
	dat.close();

	std::vector<Episode> X;
	std::ifstream Dat(f);
	Episode temp;
	int j;
	for(int i=0;i<10;i++){
		Dat>>temp;
		//std::cout<<temp<<"\n\n"<<std::endl;
		X.push_back(Episode(temp));
	}
	return X;
}


void FilePrinter::print(Season& X){
	std::ofstream out(f);
	out<<X;
	//close f
}

void ConsolePrinter::print(Season& X){
	std::cout<<X;
}
EpisodeNotFoundException::EpisodeNotFoundException(std::string ime, std::string razlog): name(ime), runtime_error(razlog){}


std::string EpisodeNotFoundException::getName() const{
	return name;
}

void Season::remove(const std::string x){
	int temp=1;
	if(0==eps.size()) EpisodeNotFoundException(x, "Prazna sezona");
	for(int i=0;i<eps.size();i++){
		if(eps[i].get_Ep_name()==x){
			eps.erase(eps.begin()+i);
			temp=0;
		}
	}
	if(1==temp){
		throw(EpisodeNotFoundException(x, "Nema epizode"));
	}
}