#pragma once

#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<iostream>
class Episode {
private:
	int N;
	double Suma;
	double max;
public:
	Episode() : N(0), Suma(0), max(0) {}
	Episode(int Nq, double Sumaq, double maxq) : N(Nq), Suma(Sumaq), max(maxq){}
	//Complex(int re, int im) : mRe(re), mIm(im) {}
	double const getAverageScore();
	double const getMaxScore();
	void addView(double R);
	int const getViewerCount();
};
double generateRandomScore();