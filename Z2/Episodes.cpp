#include "Episodes.h"

std::ostream& operator<<(std::ostream& out, const Description& rhs){
    return out<<rhs.Ep_number<<"\n"<<rhs.Ep_name<<"\n"<<rhs.Ep_duration;
} 

std::ostream& operator<<(std::ostream& out, const Episode& rhs){
    return out<<rhs.Ep_number<<"\n"<<rhs.Ep_duration<<"\n"<<rhs.Ep_name<<"\n"<<rhs.sum<<"\n"<<rhs.N<<"\n"<<rhs.max;
    
}
std::istream& operator>>(std::istream& in, Episode& rhs){
	in>>rhs.N;
	in.ignore(1);
	in>>rhs.sum;
	in.ignore(1);
	in>>rhs.max;
	in.ignore(1);
	in>>rhs.Ep_number;
	in.ignore(1);
	in>>rhs.Ep_duration;
	in.ignore(1);
	in>>rhs.Ep_name;
	in.ignore(1);
	return in;
}

double Episode::getAverageScore(){
	if (0 == N) return 0;
	return (sum) / (N);
}
double Episode::getMaxScore(){
	return max;
}

void Episode::addView(double R){
	N++;
	sum += R;
	if (R > max) max = R;
}

int Episode::getViewerCount(){
	return this->N;
}

double generateRandomScore(){
	return ((1.0 * rand()) / RAND_MAX * 10);
}


