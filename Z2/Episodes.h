#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<istream>
#include<ostream>
#include<iostream>
class Description{
		//friend std::ostream& operator<<(std::ostream& out, Episode& rhs);
	friend std::ostream& operator<<(std::ostream& out, Description& rhs);

	private:

		int Ep_number;
		int Ep_duration;
		std::string Ep_name;
	public:
	Description(int Ep_numberq=0, int Ep_durationq=0, std::string Ep_nameq=""): Ep_number(Ep_numberq), Ep_duration(Ep_durationq), Ep_name(Ep_nameq){}
	Description(Description &q): Ep_number(q.Ep_number),  Ep_duration(q.Ep_duration),  Ep_name(q.Ep_name){}
};



class Episode:public Description {
friend std::ostream& operator<<(std::ostream& out, Episode& rhs);
friend std::istream& operator>>(std::istream& in, Episode& rhs);
private:
	int N;
	double sum;
	double max;
public:
	Episode() : N(0), sum(0), max(0), Description(0,0,"") {}
	Episode(int Nq, double Sumq, double maxq, Description q) : N(Nq), sum(Sumq), max(maxq), Description(q){}
	
    double getAverageScore();
	double getMaxScore();
	void addView (double R);
	int getViewerCount();
};









double generateRandomScore();