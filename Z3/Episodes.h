#pragma once

#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<istream>
#include<fstream>
#include<ostream>
#include<iostream>
#include<string>

class Description{
	protected:
		int Ep_number;
		int Ep_duration;
		std::string Ep_name;
	public:
		Description(int, int, std::string);
		Description(Description &);
			void set_Ep_num(int); 
			void set_Ep_dur(int);
			void set_Ep_name(std::string);
			
			int get_Ep_num()const;
			int get_Ep_dur()const;
			std::string get_Ep_name()const;
};

class Episode:public Description {
private:
	int N;
	double sum;
	double max;
public:
	Episode();
	Episode(int, double, double, Description);
	void set_N(int);
	void set_sum(double);
	void set_max(double);

	int get_N() const;
	double get_sum() const;
	double get_max()const;
	
    void addView (double R);
};

class Season{
	private:
		Episode **eps;
		int num;
	public:
		Season(Episode**, int);
		Season(const Season&);
		~Season();

		Episode& operator[](int)const;
		Season& operator=(const Season&);
		double getAverageRating() const;
		double getTotalViews() const;
		Episode& getBestEpisode() const;
	};

bool operator==(const Episode&, const Episode&);

std::ostream& operator<<(std::ostream&, const Episode&);
std::istream& operator>>(std::istream&, Episode&);
std::ostream& operator<<(std::ostream&, const Description&);

double generateRandomScore();

Episode** loadEpisodesFromFile(std::string, int);