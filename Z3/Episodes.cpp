#include "Episodes.h"

//geteri i seteri
void Description::set_Ep_num(int q) { Ep_number=q; }
void Description::set_Ep_dur(int s) {Ep_duration=s;}
void Description::set_Ep_name(std::string x) {Ep_name=x;}
			
int Description::get_Ep_num()const{ return Ep_number;}
int Description::get_Ep_dur()const{ return Ep_duration;}
			
void Episode::set_N(int q) { N=q; }
void Episode::set_sum(double s) {sum=s;}
void Episode::set_max(double m) {max=m;}

int Episode::get_N() const{ return N;}
double Episode::get_sum() const{ return sum;}
double Episode::get_max()const{ return max;}
std::string Description::get_Ep_name()const{ return Ep_name;}

//kons i des
Description::Description(int Ep_numberq=0, int Ep_durationq=0, std::string Ep_nameq=""): Ep_number(Ep_numberq), Ep_duration(Ep_durationq), Ep_name(Ep_nameq){}
Description::Description(Description &q): Ep_number(q.Ep_number),  Ep_duration(q.Ep_duration),  Ep_name(q.Ep_name){}
		

Episode::Episode() : N(0), sum(0), max(0), Description(0,0,"") {}
Episode::Episode(int Nq, double Sumq, double maxq, Description q) : N(Nq), sum(Sumq), max(maxq), Description(q){}

Season::Season(const Season& ref):num(ref.num){
	eps=new Episode*[num];
	for(int i=0;i<num;i++){
		eps[i]=new Episode;
		eps[i]->set_max(ref.eps[i]->get_max());
		eps[i]->set_sum(ref.eps[i]->get_sum());
		eps[i]->set_N(ref.eps[i]->get_N());
		eps[i]->set_Ep_dur(ref.eps[i]->get_Ep_dur());
		eps[i]->set_Ep_name(ref.eps[i]->get_Ep_name());
		eps[i]->set_Ep_num(ref.eps[i]->get_Ep_num());
	}
}

Season::Season(Episode **x, int n):num(n){
	eps=new Episode*[n];
	for(int i=0;i<n;i++){
		eps[i]=new Episode;
		eps[i]->set_max(x[i]->get_max());
		eps[i]->set_sum(x[i]->get_sum());
		eps[i]->set_N(x[i]->get_N());
		eps[i]->set_Ep_dur(x[i]->get_Ep_dur());
		eps[i]->set_Ep_name(x[i]->get_Ep_name());
		eps[i]->set_Ep_num(x[i]->get_Ep_num());		
	}	
}

Season::~Season(){
	for(int i=0;i<num;i++){
		delete eps[i];
	}
	delete[] eps;
}
//operatori
bool operator==(const Episode &lhs, const Episode &rhs){
	//usporedit cu sve i samo ako se sve podudara vraca 1
	if(lhs.get_Ep_dur()==rhs.get_Ep_dur()){
		if(lhs.get_Ep_name()==rhs.get_Ep_name()){
			if(lhs.get_Ep_num()==rhs.get_Ep_num()){
				if(lhs.get_max()==rhs.get_max()){
					if(lhs.get_N()==rhs.get_N()){
						if(lhs.get_sum()==rhs.get_sum()){
							return 1;
						}
					}
				}
			}
		}
	}
	return 0;
}

Season& Season::operator=(const Season& rhs){
	if(this!=&rhs){
		num=rhs.num;
		for(int i=0;i<num; i++){
			delete *(eps+i);
		}
		delete [] eps;
		eps=new Episode*[num];
		for(int i=0;i<num;i++){
			eps[i]->set_max(rhs[i].get_max());
			eps[i]->set_sum(rhs[i].get_sum());
			eps[i]->set_N(rhs[i].get_N());
			eps[i]->set_Ep_dur(rhs[i].get_Ep_dur());
			eps[i]->set_Ep_name(rhs[i].get_Ep_name());
			eps[i]->set_Ep_num(rhs[i].get_Ep_num());
		}
	}
}

Episode& Season::operator[](int x)const{
	return *(eps[x]);
}

std::ostream& operator<<(std::ostream& out, const Description& rhs){ return out<<rhs.get_Ep_num()<<"\t"<<rhs.get_Ep_name()<<"\t"<<rhs.get_Ep_dur(); }
std::ostream& operator<<(std::ostream& out, const Episode& rhs){ return out<<rhs.get_Ep_num()<<"\t"<<rhs.get_Ep_dur()<<"\t"<<rhs.get_Ep_name()<<"\t"<<rhs.get_sum()<<"\t"<<rhs.get_N()<<"\t"<<rhs.get_max();}
std::istream& operator>>(std::istream& in, Episode& rhs){
	int tempi;
	double tempd;
	std::string temps;
	in>>tempi;
	rhs.set_N(tempi); 
	in.ignore(1);
	in>>tempd;
	rhs.set_sum(tempd);
	in.ignore(1);
	in>>tempd;
	rhs.set_max(tempd);
	in.ignore(1);
	in>>tempi;
	rhs.set_Ep_num(tempi);
	in.ignore(1);
	in>>tempi;
	rhs.set_Ep_dur(tempi);
	in.ignore(1);
	getline(in, temps);
	rhs.set_Ep_name(temps);
	return in;
}
//ostale f
void Episode::addView(double R){
	N++;
	sum += R;
	if (R > max) max = R;
}


double generateRandomScore(){ return ((1.0 * rand()) / RAND_MAX * 10); }

Episode** loadEpisodesFromFile(std::string f, int n){
	Episode** X;
	X=new Episode*[n];
	std::ifstream dat(f);
	for(int i=0;i<n;i++){
		X[i]=new Episode;
		dat>>*(X[i]);
	}
	return X;
}

double Season::getAverageRating() const{
	double sums=0;
	int Ns=0;
	for(int i=0;i<num;i++){
		sums+=eps[i]->get_sum();
		Ns+=eps[i]->get_N();
	}
	return sums/Ns;
}

double Season::getTotalViews() const{
	int tot=0;
	for(int i=0;i<num;i++){
		tot+=eps[i]->get_N();
	}
	return tot;
}

Episode& Season::getBestEpisode() const{
	double max=eps[0]->get_max();
	int q=0;
	for(int i=0;i<num;i++){
		if(eps[i]->get_max()>max){
			max=eps[i]->get_max();
			q=i;
		}
	}
	return eps[q][0];
}