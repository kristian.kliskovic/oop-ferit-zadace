﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace zadnja_zadaca_OOPa {
    public partial class Serije : Form{
        Current_search X;
        int location;
        int current_show;
        int current_season;
        int offset;
        List<Label> labels;
        public Serije() {
            InitializeComponent();
            X = new Current_search();
            labels = new List<Label>();
            labels.Add(this.label1);
            labels.Add(this.label2);
            labels.Add(this.label3);
            labels.Add(this.label4);
            labels.Add(this.label5);
            labels.Add(this.label6);
            labels.Add(this.label7);
            labels.Add(this.label8);
            labels.Add(this.label9);
            labels.Add(this.label10);
            clear_lables();
            location = 0;
            current_show = -1;
            current_season = -1;
            offset = 0;
        }
        private void Search_show_Click(object sender, EventArgs e)
        {
            location = 0;
            WebClient client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            string search = "http://api.tvmaze.com/search/shows?q=" + textBox1.Text;
            Stream data = client.OpenRead(search);
            StreamReader reader = new StreamReader(data);
            string Json_string = reader.ReadToEnd();
            try
            {
                X.Serije = JsonConvert.DeserializeObject<List<Serija>>(Json_string);
            }
            catch (JsonSerializationException r)
            {
                MessageBox.Show("Dogodila se greška prilikom otvaranja", "Greška");
                location--;
                return;
            }
            offset = 0;
            Ispis_serija();
        }
        void Ispis_serija()
        {
            if (X.Serije.Count == 0)
            {
                MessageBox.Show("Nema rezultata za traženi pojam.", "Nema rezultata");
            }
            else
            {
                clear_lables();
                for (int i = 0; i < 10; i++)
                {
                    if (X.Serije.Count >= 10 * offset + i + 1)
                    {
                        labels[i].Text = X.Serije[i + offset * 10].show.name;
                        labels[i].Text += '\n';
                        labels[i].Text += X.Serije[i + offset * 10].show.type;
                        labels[i].Text += '\n';
                        labels[i].Text += X.Serije[i + offset * 10].show.language;
                    }
                }
            }
        }

        void Ispis_sezona()
        {
            if (X.Sezone.Count == 0)
            {
                MessageBox.Show("Nema rezultata");
            }
            else
            {
                clear_lables();
                for (int i = 0; i < 10; i++)
                {
                    if (X.Sezone.Count >= 10 * offset + i + 1)
                    {
                        labels[i].Text = X.Sezone[i + offset * 10].number.ToString();
                        labels[i].Text += '\n';
                        labels[i].Text += obrada_datuma(X.Sezone[i + offset * 10].premiereDate);
                        labels[i].Text += '\n';
                        labels[i].Text += X.Sezone[i + offset * 10].name;
                    }
                }
            }
        }

        void Ispis_Epizoda()
        {
            clear_lables();
            for (int i = 0; i < 10; i++)
            {
                if (X.Epizode.Count >= 10 * offset + i + 1)
                {
                    labels[i].Text = X.Epizode[i + offset * 10].number.ToString();
                    labels[i].Text += '\n';
                    labels[i].Text += obrada_datuma(X.Epizode[i + offset * 10].airdate);
                    labels[i].Text += '\n';
                    labels[i].Text += X.Epizode[i + offset * 10].name;
                }
            }
        }
        void Ispis_Glumaca()
        {
            clear_lables();
            for (int i = 0; i < 10; i++)
            {
                if (X.Glumci.Count >= 10 * offset + i + 1)
                {
                    labels[i].Text = X.Glumci[i + offset * 10].character.name;
                    labels[i].Text += '\n';
                    labels[i].Text += X.Glumci[i + offset * 10].person.name;
                    labels[i].Text += '\n';
                    labels[i].Text += obrada_datuma(X.Glumci[i + offset * 10].person.birthday);
                }
            }
        }
        private void label1_Click(object sender, EventArgs e)
        {
            click_label(0);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            click_label(1);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            click_label(2);
        }

        private void label4_Click(object sender, EventArgs e)
        {
            click_label(3);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            click_label(4);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            click_label(5);
        }

        private void label7_Click(object sender, EventArgs e)
        {
            click_label(6);
        }

        private void label8_Click(object sender, EventArgs e)
        {
            click_label(7);
        }

        private void label9_Click(object sender, EventArgs e)
        {
            click_label(8);
        }

        private void label10_Click(object sender, EventArgs e)
        {
            click_label(9);
        }

        void click_label(int n)
        {
            if (location == 0)
            {
                current_show = n + offset * 10;
                location = 1;
                Click_Show(n + offset * 10);

            }
            else if (location == 1)
            {
                current_season = n + offset * 10;
                location = 2;
                Click_Season(n + offset * 10);
            }
        }

        void Click_Show(int n)
        {
            if (X.Serije.Count > n)
            {
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                string search = "http://api.tvmaze.com/shows/" + X.Serije[n].show.id.ToString() + "/seasons";

                Stream data = client.OpenRead(search);
                StreamReader reader = new StreamReader(data);
                string Json_string = reader.ReadToEnd();
                try
                {
                    X.Sezone = JsonConvert.DeserializeObject<List<Sezona>>(Json_string);
                }
                catch (JsonSerializationException e)
                {
                    MessageBox.Show("Dogodila se greška prilikom otvaranja", "Greška");
                    location--;
                    return;
                }
                offset = 0;
                Ispis_sezona();
            }
        }
        void Click_Season(int n)
        {
            if (X.Sezone.Count > n)
            {
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                string search = "http://api.tvmaze.com/seasons/" + X.Sezone[n].id.ToString() + "/episodes";
                Stream data = client.OpenRead(search);
                StreamReader reader = new StreamReader(data);
                string Json_string = reader.ReadToEnd();
                try
                {
                    X.Epizode = JsonConvert.DeserializeObject<List<Epizoda>>(Json_string);
                }
                catch (JsonSerializationException e) {
                    MessageBox.Show("Ne mogu otvoriti", "Greška");
                    location--;
                    return;
                }
                offset = 0;
                Ispis_Epizoda();
            }
        }

        void clear_lables()
        {
            for (int i = 0; i < 10; i++)
            {
                labels[i].Text = "";
            }
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            if (location != 0) offset = 0;
            if (location > 2 && location <100) location -= 10;
            else if (location > 100) location -= 100;
            else --location;

            if (location < 0) location = 0;
            else if (location == 0)
            {
                Ispis_serija();
            }
            else if (location == 1)
            {
                Ispis_sezona();
            }
            else if (location == 2)
            {
                Ispis_Epizoda();
            }
        }

        private void previous10_Click(object sender, EventArgs e)
        {
            if (location > 100) return;
            offset -= 1;
            if (offset < 0) offset = 0;
            else switch (location)
            {
                case 0:
                    Ispis_serija();
                    break;
                case 1:
                    Ispis_sezona();
                    break;
                case 2:
                    Ispis_Epizoda();
                    break;
                default:
                    Ispis_Glumaca();
                    break;
            }
        }

        private void next10_Click(object sender, EventArgs e)
        {
            if (location > 100) return;
            switch (location)
            {
                case 0:
                    if (X.Serije.Count > (offset + 1) * 10)
                    {
                        offset++;
                        Ispis_serija();
                    }
                    break;
                case 1:
                    if (X.Sezone.Count > (offset + 1) * 10)
                    {
                        offset++;
                        Ispis_sezona();
                    }
                    break;
                case 2:
                    if (X.Epizode.Count > (offset + 1) * 10)
                    {
                        offset++;
                        Ispis_Epizoda();
                    }
                    break;
                default:
                    if (X.Glumci.Count > (offset + 1) * 10)
                    {
                        offset++;
                        Ispis_Glumaca();
                    }
                    break;
            }
        }

        private void show_actors_Click(object sender, EventArgs e)
        {
            if (location >= 1&&location<=2)
            {
                location += 10;
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                string search = "http://api.tvmaze.com/shows/" + X.Serije[current_show].show.id.ToString() + "/cast";
                Stream data = client.OpenRead(search);
                StreamReader reader = new StreamReader(data);
                string Json_string = reader.ReadToEnd();
                X.Glumci = JsonConvert.DeserializeObject<List<Glumac>>(Json_string);
                if (X.Glumci.Count == 0)
                {
                    location -= 10;
                    MessageBox.Show("Nemam prikaz glumaca za tu seriju");
                }
                Ispis_Glumaca();
            }
        }

        private void button_informacije_Click(object sender, EventArgs e)
        {
            if (location > 0)
            {
                if (location < 100) location += 100;
                clear_lables();
                labels[0].Text = obrada_opisa(X.Serije[current_show].show.summary);
            }
        }

        private string obrada_opisa(string text)
        {
            for (int i = 0; i < text.Length; i++) {
                if (text[i] == '/')
                {
                    text = text.Remove(i, 1);
                }
            }
            for (int i = 0; i<text.Length; i++)
            {
                if (text[i] == '<')
                {
                    text=text.Remove(i, 3);
                    i-=2;
                    if (i < 0) i = -1;
                }
            }
            int brojac = 0;
            for (int i = 0; i < text.Length; i++)
            {
                brojac++;
                if (brojac > 85)
                {
                    if (text[i] == ' ')
                    {
                        text=text.Insert(i + 1, "\n");
                        brojac = 0;
                    }
                }
            }
            return text;
        }
        private string obrada_datuma(string datum)
        {
            if (datum == null) return "";
            datum = datum.Replace("-",".");
            return datum;
        }

    }

    //objekti koji definiraju Seriju
    public class Schedule{
        public string time { get; set; }
        public List<object> days { get; set; }
    }

    public class Rating{
        public double? average { get; set; }
    }

    public class Country{
        public string name { get; set; }
        public string code { get; set; }
        public string timezone { get; set; }
    }

    public class Network{
        public int id { get; set; }
        public string name { get; set; }
        public Country country { get; set; }
    }

    public class Country2
    {
        public string name { get; set; }
        public string code { get; set; }
        public string timezone { get; set; }
    }

    public class WebChannel{
        public int id { get; set; }
        public string name { get; set; }
        public Country2 country { get; set; }
    }

    public class Externals{
        public int? tvrage { get; set; }
        public int? thetvdb { get; set; }
        public string imdb { get; set; }
    }

    public class Image{
        public string medium { get; set; }
        public string original { get; set; }
    }

    public class Self{
        public string href { get; set; }
    }

    public class Previousepisode{
        public string href { get; set; }
    }

    public class Nextepisode{
        public string href { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
        public Previousepisode previousepisode { get; set; }
        public Nextepisode nextepisode { get; set; }
    }

    public class Show
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string language { get; set; }
        public List<string> genres { get; set; }
        public string status { get; set; }
        public int? runtime { get; set; }
        public string premiered { get; set; }
        public string officialSite { get; set; }
        public Schedule schedule { get; set; }
        public Rating rating { get; set; }
        public int weight { get; set; }
        public Network network { get; set; }
        public WebChannel webChannel { get; set; }
        public Externals externals { get; set; }
        public Image image { get; set; }
        public string summary { get; set; }
        public int updated { get; set; }
        public Links _links { get; set; }
    }

    public class Serija
    {
        public double score { get; set; }
        public Show show { get; set; }

        [JsonConstructor]
        Serija(double jedan, Show X)
        {
            show = X;
            score = jedan;
        }
    }

    //Objekti koji definiraju Sezonu
    public class Countryy
    {
        public string name { get; set; }
        public string code { get; set; }
        public string timezone { get; set; }
    }

    public class Networkk
    {
        public int id { get; set; }
        public string name { get; set; }
        public Country country { get; set; }
    }

    public class Selff
    {
        public string href { get; set; }
    }

    public class Linkss
    {
        public Selff Selff { get; set; }
    }

    public class Sezona
    {
        public int id { get; set; }
        public string url { get; set; }
        public int number { get; set; }
        public string name { get; set; }
        public int? episodeOrder { get; set; }
        public string premiereDate { get; set; }
        public string endDate { get; set; }
        public Networkk Networkk { get; set; }
        public object webChannel { get; set; }
        public object image { get; set; }
        public object summary { get; set; }
        public Linkss _Linkss { get; set; }
    }
    //klase koje definiraju epizodu


    public class _Image
    {
        public string medium { get; set; }
        public string original { get; set; }
    }

    public class _Self
    {
        public string href { get; set; }
    }

    public class _Links
    {
        public _Self self { get; set; }
    }

    public class Epizoda
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public int season { get; set; }
        public int number { get; set; }
        public string airdate { get; set; }
        public string airtime { get; set; }
        public DateTime airstamp { get; set; }
        public int runtime { get; set; }
        public _Image image { get; set; }
        public string summary { get; set; }
        public _Links _links { get; set; }
    }

    //klase koje definiraju glumce
    /*public class Country
    {
        public string name { get; set; }
        public string code { get; set; }
        public string timezone { get; set; }
    }*/

    /*public class Image
    {
        public string medium { get; set; }
        public string original { get; set; }
    }*/

    /*public class Self
    {
        public string href { get; set; }
    }*/

    /*public class Links
    {
        public Self self { get; set; }
    }*/

    public class Person
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public Country country { get; set; }
        public string birthday { get; set; }
        public object deathday { get; set; }
        public string gender { get; set; }
        public Image image { get; set; }
        public Links _links { get; set; }
    }

    public class Image2
    {
        public string medium { get; set; }
        public string original { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
    }

    public class Character
    {
        public int id { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public Image2 image { get; set; }
        public Links2 _links { get; set; }
    }

    public class Glumac
    {
        public Person person { get; set; }
        public Character character { get; set; }
        public bool self { get; set; }
        public bool voice { get; set; }
    }



    //end objekata

    public class Current_search
    {
        public List<Serija> Serije { get; set; }
        public List<Sezona> Sezone { get; set; }
        public List<Epizoda> Epizode { get; set; }
        public List<Glumac> Glumci { get; set; }
        public Current_search()
        {
            Serije = new List<Serija>();
            Sezone = new List<Sezona>();
            Epizode = new List<Epizoda>();
            Glumci = new List<Glumac>();
        }
    }
}